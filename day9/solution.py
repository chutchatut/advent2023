x = """0 3 6 9 12 15
1 3 6 10 15 21
10 13 16 21 30 45"""

def get_diff(row):
    out = []
    for i in range(1, len(row)):
        out.append(row[i] - row[i-1])
    return out

def pred(row):
    levels = [row]
    while len(levels[-1]) > 1:
        levels.append(get_diff(levels[-1]))
    x = 0
    for *_, last in levels[::-1]:
        x += last
    return x

s = 0
for line in x.split('\n'):
    line = [int(i) for i in line.split()]
    line.reverse()
    s += (pred(line))
print(s)