seeds = "79 14 55 13"

seed_to_soil = """50 98 2
52 50 48"""

soil_to_fertilizer = """0 15 37
37 52 2
39 0 15"""

fertilizer_to_water = """49 53 8
0 11 42
42 0 7
57 7 4"""

water_to_light = """88 18 7
18 25 70"""

light_to_temperature = """45 77 23
81 45 19
68 64 13"""

temperature_to_humidity = """0 69 1
1 0 69"""

humidity_to_location = """60 56 37
56 93 4"""


class Segment():
    def __init__(self, start, end):
        self.start = start
        self.end = end

    def __repr__(self) -> str:
        return f'<start={self.start}, end={self.end}>'

    def __len__(self):
        return max(0, self.end - self.start)
    
    def __lt__(self, other):
        return self.start < other.start
    
    def union(self, other):
        return Segment(min(self.start, other.start), max(self.end, other.end))

    def intersect(self, other):
        return Segment(max(self.start, other.start), min(self.end, other.end))

    def is_intersect(self, other):
        return len(self.intersect(other)) > 0

    def difference(self, other) -> list['Segment']:
        out = []
        if self.start < other.start:
            out.append(Segment(self.start, min(self.end, other.start)))
        if self.end > other.end:
            out.append(Segment(max(self.start, other.end), self.end))

        return out


seeds = seeds.split()
seeds = [Segment(int(seeds[i]), int(seeds[i])+int(seeds[i+1])) for i in range(0, len(seeds), 2)]

def apply_filter(my_filter, s):
    new_s = []
    unprocess_s = list(s)

    for line in my_filter.split('\n'):
        dest, src, size = map(int, line.split())
        src_seg = Segment(src, src+size)
        for i in range(len(unprocess_s)-1,-1,-1):
            if not unprocess_s[i].is_intersect(src_seg):
                continue
            inter = unprocess_s[i].intersect(src_seg)
            diff = unprocess_s[i].difference(src_seg)

            inter.start += dest-src
            inter.end += dest-src
            new_s.append(inter)
            del unprocess_s[i]
            unprocess_s.extend(diff)

    return new_s + unprocess_s

def merge_seeds(s):
    s.sort()
    new_s = []
    i = 0
    for j in range(1, len(s)):
        if not s[i].is_intersect(s[j]):
            new_s.append(s[i].union(s[j-1]))
            i = j
    new_s.append(s[i].union(s[j]))
    return new_s

seeds = apply_filter(seed_to_soil, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(soil_to_fertilizer, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(fertilizer_to_water, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(water_to_light, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(light_to_temperature, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(temperature_to_humidity, seeds)
seeds = merge_seeds(seeds)
seeds = apply_filter(humidity_to_location, seeds)
print('segments =', merge_seeds(seeds))
print('min =', min(s.start for s in seeds))
