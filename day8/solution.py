# data is kinda special since it always goes back to the same A after Z
# so the answer is just to take the lcm of all the loops

instruction = "LR"

my_map_repr = """11A = (11B, XXX)
11B = (XXX, 11Z)
11Z = (11B, XXX)
22A = (22B, XXX)
22B = (22C, 22C)
22C = (22Z, 22Z)
22Z = (22B, 22B)
XXX = (XXX, XXX)"""

my_map = dict()

for my_map_row in my_map_repr.split('\n'):
    src, dest = my_map_row.split(' = ')
    left, right = dest.split(', ')
    my_map[src] = (left[1:], right[:-1])

current_nodes = list(filter(lambda x: x[-1]=='A', my_map.keys()))

def find_cycle(node):
    # return the offset and cycle size
    step = 0
    i = 0
    offsets = []
    first_occurance = None
    while True:
        step += 1
        node = my_map[node][0 if instruction[i] == 'L' else 1]
        i = (i+1)%len(instruction)
        if node[-1] == 'Z':
            offsets.append(step)
            if node == first_occurance:
                return offsets, step - offsets[0]
            if first_occurance is None:
                first_occurance = node

for node in current_nodes:
    print(find_cycle(node))


from math import gcd
from functools import reduce

def lcm(a, b):
    return a * b // gcd(a, b)

x = [2, 3, 5] # put loop size here
print(reduce(lcm, x))