from collections import Counter, defaultdict

x = """32T3K 765
T55J5 684
KK677 28
KTJJT 220
QQQJA 483"""

with open('data.txt') as f:
    x = f.read()

def four_of_a_kind():
    pass

order = 'AKQT987654321J'
order = {k: i + 1 for i, k in enumerate(reversed(order))}

def get_strength(cards):
    count = Counter(cards)
    counts = []
    for k, v in count.items():
        if k != 'J':
            counts.append(v)
    counts.append(0)
    counts.sort(reverse=True)
    counts[0] += count['J']

    match(counts):
        case [5, *_]:
            return 6
        case [4, *_]:
            return 5
        case [3, 2, *_]:
            return 4
        case [3, *_]:
            return 3
        case [2, 2, *_]:
            return 2
        case [2, *_]:
            return 1
        case _:
            return 0

power_and_bid = []

for row in x.split('\n'):
    cards, bid = row.split()
    power_and_bid.append(((get_strength(cards), *map(order.__getitem__,cards)), int(bid)))
    
print(power_and_bid)
power_and_bid.sort()

s = 0
for i, pb in enumerate(power_and_bid):
    s += (i+1)*pb[1]
print(s) 