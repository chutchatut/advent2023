from math import floor, ceil

times = "63789468"
dists = "411127420471035"

# times = "7  15   30"
# dists = "9  40  200"

times = list(map(int, times.split()))
dists = list(map(int, dists.split()))

def get_solution(time, dist):
    # new_dist = (time-hold) * hold >= dist
    # hold^2 - time*hold + dist <= 0
    # hold = (time +- sqrt(time^2 - 4*dist)) / 2
    x = (time*time - 4*dist)**.5
    a = ceil((time - x) / 2)
    b = floor((time + x) / 2)
    if (time-a) * a <= dist:
        a += 1
    if (time-b) * b <= dist:
        b -= 1
    print('bound =', a, b)
    return max(b-a+1, 0)
    
    
i = 1

for t, d in zip(times, dists):
    i *= get_solution(t, d)

print(i)